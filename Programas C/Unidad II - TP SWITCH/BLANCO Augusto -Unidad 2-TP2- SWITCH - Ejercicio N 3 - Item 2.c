#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�3
	//Item: 2.
	
	int valor1, valor2, resultado, selecOpera;
	
	printf("Ingrese un primer valor entero: ");
	scanf("%i", &valor1);
	
	printf("\nIngrese un segundo valor entero: ");
	scanf("%i", &valor2);
	
	printf("\nSeleccione la operacion que desea realizar,\ningrese el valor entero correspondiente para cada operacion:\n1: Suma(+).\n2: Resta(-).\n3: Multiplicacion(*).\n4: Division(/).\n" );
	scanf("%i",&selecOpera);
	
	switch(selecOpera) 
	{
	case 1: 
		resultado = valor1+valor2;
		printf("\nEl resultado de la operacion suma es: %i", resultado);
		break;
	case 2: 
		resultado = valor1-valor2;
		printf("\nEl resultado de la operacion resta es: %i", resultado);
		break;
	case 3: 
		resultado = valor1*valor2;
		printf("\nEl resultado de la operacion multiplicacion es: %i", resultado);
		break;
	case 4:
		resultado = valor1/valor2;
		printf("\nEl resultado de la operacion division es: %i", resultado);
		break;
	default: printf("\nIngrese un valor que coincida con las operaciones.");
	}
	
	getch();
	return 0;
}
