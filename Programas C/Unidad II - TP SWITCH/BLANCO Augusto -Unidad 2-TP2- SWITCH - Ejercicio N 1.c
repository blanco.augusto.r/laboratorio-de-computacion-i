/* Ejemplo de programa para la sentencia switch case */

#include <stdio.h>
#include <conio.h>
// FALTA LA DECLARACION PARA LA FUNCION DE LA INICIALIZACION DEL PROGRAMA "int"
int main(int argc, char *argv[])
{
	float precio_libro, precio_neto, descuento;
	// SE PUEDE AHORRAR UNA LINEA COLOCANDOLO EN EL PRIMER FLOAT: float descuento;
	int cod_cliente;
	// FALTA COMILLAS: printf("Introduzca el precio del libro:\n);
	printf("Introduzca el precio del libro: \n");
	scanf("%f", &precio_libro);
	// FALTA COMILLAS: printf("Introduzca el c�digo del cliente:\n);
	printf("Introduzca el codigo del cliente: \n");
	// LA CADENA DE CONTROL DEBERIA DE SER %i PARA NUMEROS ENTEROS SIN DECIMAL: scanf("%d", &cod_cliente);
	scanf("%i", &cod_cliente);
	
	// LA CONDICION DEL SWITCH "(codigo)" NO COINCIDE CON cod_cliente
	//switch (codigo) 
	switch(cod_cliente) {
		case 1 : /* Clientes registrados */
						descuento = 0.1;
						precio_neto = precio_libro - (precio_libro * descuento);
						break;
		case 2 : /* Mayoristas */
						descuento = 0.15;
						precio_neto = precio_libro - (precio_libro * descuento);
						break;
		case 3: /* Empleados de la empresa */
						descuento = 0.17;
						precio_neto = precio_libro - (precio_libro * descuento);
						break;
		default: /* Nuevos clientes */
						descuento = 0.05;
						precio_neto = precio_libro - (precio_libro * descuento);
						// COMO EL DEFAULT ES LA ULTIMA OPCION, NO REQUIERE DE COLOCAR EL "break;"
						//break;
	}
	
	printf("El precio neto del libro es %f\n", precio_neto);
	// PARA QUE LA FUNCION getch SE EJECUTE CORRECTAMENTE, 1ero: TIENE QUE INCLUIR LA LIBRERIA Y 2do: DESPUES DE LA PALABRA RESERVADA getch VA LOS PARENTESIS
	getch();
	// FALTA EL RETURN
	return 0;
}
