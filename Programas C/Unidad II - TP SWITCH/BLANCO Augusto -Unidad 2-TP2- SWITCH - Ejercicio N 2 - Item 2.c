#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�2
	//Item: 2.
	
	int num1, num2, resultOpera, selector;
	
	printf("Ingrese un primer valor entero: ");
	scanf("%i", &num1);
	
	printf("\nIngrese un segundo valor entero: ");
	scanf("%i", &num2);
	
	printf("\nSeleccione la operacion que desea realizar,\ningrese el valor entero correspondiente para cada operacion:" );
	printf("\n1: Suma(+).");
	printf("\n2: Resta(-).");
	printf("\n3: Multiplicacion(*).");
	printf("\n4: Division(/).");
	printf("\n");
	scanf("%i",&selector);
	
	switch(selector) 
	{
	case 1: 
					resultOpera = num1+num2;
					printf("\nEl resultado de la operacion suma es: %i", resultOpera);
					break;
	case 2: 
					resultOpera = num1-num2;
					printf("\nEl resultado de la operacion resta es: %i", resultOpera);
					break;
	case 3: 
					resultOpera = num1*num2;
					printf("\nEl resultado de la operacion multiplicacion es: %i", resultOpera);
					break;
	case 4:
					resultOpera = num1/num2;
					printf("\nEl resultado de la operacion division es: %i", resultOpera);
					break;
	default: printf("\nIngrese un valor que coincida con las operaciones.");
	}
	
	getch();
	return 0;
}
