#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�3
	//Item: 3-
	int num1, num2, resultOpera;
	char caracter;
	
	printf("Ingrese un primer valor entero: ");
	scanf("%i", &num1);
	
	printf("\nIngrese un segundo valor entero: ");
	scanf("%i", &num2);
	
	printf("\nSeleccione la operacion que desea realizar,\ningresando la inicial del tipo de operacion: ");
	printf("\nSuma(+).");
	printf("\nResta(-).");
	printf("\nMultiplicacion(*).");
	printf("\nDivision(/).");
	printf("\n");
	
	scanf(" %c",&caracter);
	
	switch(caracter)
	{
		case 's': 
						resultOpera = num1+num2;
						printf("\nEl resultado de la operacion suma es: %i", resultOpera);
						break;
		case 'S': 
						resultOpera = num1+num2;
						printf("\nEl resultado de la operacion suma es: %i", resultOpera);
						break;
		case 'r': 
						resultOpera = num1-num2;
						printf("\nEl resultado de la operacion resta es: %i", resultOpera);
						break;
		case 'R': 
						resultOpera = num1-num2;
						printf("\nEl resultado de la operacion resta es: %i", resultOpera);
						break;
		case 'm': 
						resultOpera = num1*num2;
						printf("\nEl resultado de la operacion multiplicacion es: %i", resultOpera);
						break;
		case 'M': 
						resultOpera = num1*num2;
						printf("\nEl resultado de la operacion multiplicacion es: %i", resultOpera);
						break;
		case 'd':
						resultOpera = num1/num2;
						printf("\nEl resultado de la operacion division es: %i", resultOpera);
						break;
		case 'D':
						resultOpera = num1/num2;
						printf("\nEl resultado de la operacion division es: %i", resultOpera);
						break;
		default: printf("\nIngrese una inicial que coincida con las operaciones.");
	}
	
	getch();
	return 0;
}
