#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[])
{
	//Ejercicio N�3
	//Item: B
	
	int i, j, k;
	
	for( i=3 ; i>0 ; i--)
	{
		for( j=1 ; j<=i ; j++){
			for( k=i ; k>=j ; k--)
			{
				printf("%d %d %d \n", i, j, k);
			}
		}
	}
	
	/*
	Los datos de salida son:
	3 1 3
	3 1 2
	3 1 1
	3 2 3
	3 2 2
	3 3 3
	2 1 2
	2 1 1
	2 2 2
	1 1 1
	*/
	
	getch();
	return 0;
}
