#include <stdio.h>
#include <conio.h>

int numPrimo(int num);

int main(int argc, char *argv[])
{
	//Ejercicio N�6
	
	int num, primo = 0, contPrimo;
	
	contPrimo = 0;
	
	for (int i = 1; i > 0; i++ ){
		
		printf("Ingrese numero: ");
		scanf ("%d", &num);
		
		if (num == 0) break;
			
		primo = numPrimo(num);
		
		if (primo == 1) contPrimo++;
		
	}
	
	printf("Cantidad de numeros primos ingresados: %d", contPrimo);
	
	getch();
	return 0;
}
	
int numPrimo(int num){
	
	int div = 2;
	int primo = 1;
	
	while (( div<num ) && ( primo!=0) ){
		
		if (num % div==0 ) primo = 0;
		
		div++;
	}
	
	return primo;
	
}
