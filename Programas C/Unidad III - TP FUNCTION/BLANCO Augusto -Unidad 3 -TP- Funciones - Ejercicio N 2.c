#include <stdio.h>
#include <conio.h>

void multiplicacion();

int main(int argc, char *argv[]) {
	//Ejercicio N�2:
	
	printf("Ingrese la cantidad de numeros enteros que desee.\n\n");
	
	multiplicacion();
	
	getch();
	return 0;
}

void multiplicacion() {
	
	char yesornot;
	int mult, i=1,num;
	
	mult = 1;
	do {
		printf("Ingrese un valor n�%i:", i); scanf("%i", &num); printf("\n");
		
		mult = mult * num; 
		
		i++;
		printf(" �Quiere seguir ingresando numeros para realizar la multiplicacion?\n");
		printf(" \tIngrese la letra >> s << para seguir.\n\tIngrese la letra >> n << para terminar.\n Digite la letra y presione enter: "); scanf(" %c", &yesornot); printf("\n");
	} while(yesornot == 's');
	
	printf("El valor de la multiplicacion entre los valores ingresados es: %i", mult);
}

