#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int mayor=0, menor=999999999, cantValores, numIngres, suma=0, i;
float promed;

int valorMinimo();
int valorMaximo();
int sumaTot();
int promedio();

int main(int argc, char *argv[]) {
	//Ejercicio N�3
	
	printf("Indice la cantidad de valores que de desea ingresar: "); scanf("%i", &cantValores); printf("\n");
	
	for( i=1 ; i<=cantValores ; i++) {
		
		printf("Valor N� %i: ", i); scanf("%i", &numIngres); printf("\n");
		
		valorMinimo(numIngres);
		valorMaximo(numIngres);
		sumaTot(numIngres);
		promedio(cantValores);
		
	}
	
	system("cls");
	
	printf(" El valor maximo es: %i", mayor); printf("\n");
	printf(" El valor minimo es: %i", menor); printf("\n");
	printf(" El promedio de los valores ingresados son: %.2f", promed); printf("\n");
	
	getch();
	return 0;
}



int valorMaximo(int numIngres) {
	
	if (numIngres > mayor)
	{
		mayor = numIngres;
	}
	
	return mayor;
}

int valorMinimo(int numIngres) {
	
	if (numIngres < menor)
	{
		menor = numIngres;
	}
	
	return menor;
}

int sumaTot(int numIngres) {
	
	suma = suma + numIngres;
	
	return suma;
}

int promedio(int cantValores) {
	
	promed = suma/cantValores;
	
	return promed;
}
