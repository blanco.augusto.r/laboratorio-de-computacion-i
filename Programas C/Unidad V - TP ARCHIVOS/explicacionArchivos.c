#include <stdio.h>
#include <stdlib.h>


// Lo primero que se tiene que hacer es crear un archivo de texto con un nombre simple y la extension txt
// Este archivo de texto tiene que estar en la misma carpeta que el archivo para que funcione,
// Sino se tiene que agregar la extension de busqueda como si buscaramos una carpeta normal
// Recordar que el archivo de texto tiene que estar siempre cerrado


int main() 
{
	char aux;
	char aux2[100];
	
	// El * es un puntero, y cuadno colocamos la palabra FILE estamos creando un archivo (FILE *nombreArchivo)
	FILE *fichero;

	// Hay que inicializar la variable a la que se le apunto el fichero
	// Luego escribir la funcion fopen() para abrir el archivo
	// Dentro de la funcion se escribe el nombre y la extension del archvio
	// Despues de una coma, se escribe el modo en el que lo queremos abrir por ejemplo "r" que es modo lectura.
	fichero = fopen("explicacionArchivos.txt","r");
	
	// Luego tenemos que comprobar si el archivo se abrio correctamente:
	if( fichero == NULL)
	{
		printf("No se a podido abrir el fichero.\n");
		exit(1);
	}
	
	while( aux != EOF){
		aux = fgetc(fichero);
		printf("%c",aux);
	}
	
	printf("\n");
	
	// Acordase de siempre cerrar el fichero para que no entre ningun tipo de dato corrupto
	fclose(fichero);
	
	// Empezar otro programa
	//  Otra forma de mostrar por pantalla todo lo que se escribio en el archivo es ingresandolo en una cadena
	// Con una longuitud bastante grande para que cabe todo
	
	fichero = fopen("explicacionArchivos.txt","r");
	if( fichero == NULL)
	{
		printf("No se a podido abrir el fichero.\n");
		exit(1);
	}
	
	while(!feof(fichero))
	{
		fgets(aux2,100,fichero);
		printf("%s",aux2);
	}
	printf("\n");

	fclose(fichero);
	
	
	
	
	system("pause");
	return 0;
}
