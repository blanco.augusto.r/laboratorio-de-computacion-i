#include <stdio.h>
#include <conio.h>


int main(int argc, char *argv[]) {

	int numIngres, cont=1, contPrimos=0, resto;
	
	printf("\t\tPROGRAMA: SI ES PRIMO UN NUMERO\n\n");
	printf("Ingrese un numero entero: ");
	scanf("%i", &numIngres);
	
	while(cont <= numIngres){
		resto = numIngres%cont;
		printf("\nResto es: %i", resto);
		if(resto == 0)
		{
			contPrimos++;
		}
		cont++;
	}
	
	if(contPrimos == 2){
		printf("\nEl numero %i es primo.", numIngres);
	} else {
		printf("\nEl numero %i no es primo.", numIngres);
	}
	
	printf("\nResto: %i", resto);
	getch();
	return 0;
}

