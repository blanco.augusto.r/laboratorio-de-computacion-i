#include <stdio.h>
#include <conio.h>


int main(int argc, char *argv[]) 
{
	//Ejercicio N�5
	
	float radio, superficie, pi;
		
	printf("Ingrese el valor del radio: ");
	scanf("%f",&radio);
	
	printf("\nIngrese el valor de pi con dos decimales: ");
	scanf("%f", &pi);
	
	while(pi==3.14) {
		printf("\nValor incorrecto, recuerde que pi vale 3,14159 26535...");
		printf("\nVuelva a ingresar el valor de pi solamente con dos decimales: ");
		scanf("%f",&pi);
	}
	
	superficie = pi *(radio*radio);
	
	printf("\nLa superficie del circulo es: %.2f",superficie);
	
	getch();
	return 0;
}
