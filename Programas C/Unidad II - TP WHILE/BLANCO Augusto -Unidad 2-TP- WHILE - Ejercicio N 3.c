#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�3
	
	int dividendo, divisor, divResta, cociente=0;
	
	printf("Ingrese un valor para el dividendo: ");
	scanf("%d", &dividendo);
	
	printf("\nIngrese un valor para el divisor: ");
	scanf("%d", &divisor);
	
	divResta = dividendo;
	if((dividendo%2)!=0){
		while(divResta > 1) 
		{
			divResta = divResta - divisor;
			cociente++;
		}
	} else {
		while(divResta > 0) 
		{
			divResta = divResta - divisor;
			cociente++;
		}
	}
	
	printf("\nEl residuo de la division es: %d",divResta);
	
	printf("\nEl cociente de la division es: %d", cociente);
	
	getch();
	return 0;
}
