#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]){
	//Ejercicio N�6:
	
	float total, precioUnit;
	int cont, cantLinea;
	
	printf ("Ingrese cantidad de lineas que contiene la factura: ");
	scanf("%d",&cantLinea);
	
	cont = 1; 
	total= 0;
	
	while (cont<=cantLinea)
	{
		printf("\nIngrese precio del producto: $");
		scanf("%f",&precioUnit);
		total = total+precioUnit;
		
		if (cont<cantLinea)
		{
			printf("\nEl total parcial es: $%.2f\n\n", total);
		}
		
		cont++;
		
	}
	
	printf("\nEl total a pagar es: $%.2f", total);
	
	getch();
	return 0;
}
