#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�2
	
	int multiplicando, multiplicador, producto=0, cont=1;
	
	printf("Ingrese un valor para multiplicar: ");
	scanf("%d", &multiplicando);
	
	printf("\nIngrese el multiplicador que desee elegir: ");
	scanf("%d", &multiplicador);
	
	while(cont <= multiplicador) 
	{
		producto = producto + multiplicando;
		cont++;
	}
	
	printf("\nEl resultado de %d por %d es: %d",multiplicando, multiplicador, producto);

	getch();
	return 0;
}
