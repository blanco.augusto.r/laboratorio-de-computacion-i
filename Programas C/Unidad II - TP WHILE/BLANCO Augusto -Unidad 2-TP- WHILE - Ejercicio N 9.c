#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�9:
	
	float milAgxDia=0,total=0, sup=0,inf=99999999, promedioAg;
	int cont=1,a,f=0;
	
	printf("Ingresa la cantidad de dias del mes, que desea ingresar: ");
	scanf("%d",&a);
	
	while(cont<=a){
		
		printf("Ingresar los milimetros de agua caida en el dia %d: ",cont);
		scanf("%f",&milAgxDia);
		total=total+milAgxDia;
		
		cont++;
		if(milAgxDia>sup){
			sup=milAgxDia;
			f=cont;
		}
		if(milAgxDia<inf){
			inf=milAgxDia;
			f=cont;
		}
	}
	
	promedioAg = total/a;
	
	printf("\nEl promedio de milimetros de la lluvia por dia es: %.2f",promedioAg);
	printf("\nEl dia de mayor lluvia fue el dia %d con un total de: %.2fmm.",f, sup);
	printf("\nEl dia de menor lluvia fue el dia %d con un total de: %.2fmm.",f, inf);
	
	getch();
	return 0;
}
