#include <stdio.h>
#include <conio.h>

int main(){
	//Ejercicio N�7:
	
	int limiteInicial, limiteFinal, dif;
	
	printf("\nIngrese el valor del limite inicial: ");
	scanf("%d",&limiteInicial);
	
	printf("\nIngrese el valor del limite final: ");
	scanf("%d",&limiteFinal);
	
	while(limiteFinal<limiteInicial){
		
		printf("\nEl limite inicial es mayor que el limite final, vuelva a ingresar los datos.\n");
		printf("\nLimite inferior: ");
		scanf("%d",&limiteInicial);
		
		printf("\nLimite superior: ");
		scanf("%d",&limiteFinal);
		
	}
		
	dif=limiteFinal-limiteInicial;
	
	printf("La cantidad de numeros comprendidos en el intervalo son: %d",dif);
	
	getch();
	return 0;
}
