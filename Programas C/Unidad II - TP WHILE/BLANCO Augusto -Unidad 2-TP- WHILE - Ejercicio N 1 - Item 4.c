#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�1:
	//Item: 4.
	
	int valor, promedio, suma=0, cont = 1, cantSiMenosQuin=0, cantNoMenosQuin=0;
	
	while(cont<=5) {
		
		printf("\nIngrese un valor entero: ");
		scanf("%i",&valor);
		
		printf("\nEl valor que ingreso es: %i\n",valor);
		suma = suma+valor;
		printf("\nEl valor de la suma con el valor anterior es: %i", suma);
		
		if(valor<=15) {
			cantSiMenosQuin++;
		}else {
			cantNoMenosQuin++;
		}
		
		cont++;
	}
	printf("\n***************************************************************\n");
	
	promedio = suma/5;
	printf("\nEl promedio total es: %i", promedio);
	
	printf("\nLa cantidad de numeros menores que 15 son: %i", cantSiMenosQuin);
	printf("\nLa cantidad de numeros que son mayores que 15 son: %i", cantNoMenosQuin);
	
	getch();
	return 0;
}
