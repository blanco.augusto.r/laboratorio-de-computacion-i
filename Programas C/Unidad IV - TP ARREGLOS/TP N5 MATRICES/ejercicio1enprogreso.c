#include <stdio.h>
#include <conio.h>

void imprimirMatriz(int matrices[][2]);
void sumatoriaElementosMatriz(int matrices[][2]);
void esPrimo(int matrices[][2]);

int suma=0;

int main(int argc, char *argv[]) 
{
	//Matriz
	
	int matriz[2][2] = {{1,2},{3,4}};
	float promedio;
	
	imprimirMatriz (matriz);
	printf("\n");
	sumatoriaElementosMatriz(matriz);
	
	promedio = suma/4;
	
	printf("\n\nEl promedio de la suma de cada elemento de la matriz es: %.2f", promedio);
	printf("\n\n");
	esPrimo(matriz);
	getch();
	return 0;
}

void imprimirMatriz(int matrices[][2])
{
	int i, j;
	
	for( i=0 ; i<2 ; i++)
	{
		for( j=0 ; j<2 ; j++)
		{
			printf("[%i] ", matrices[i][j]);
		}
		printf("\n");
	}
}

void sumatoriaElementosMatriz(int matrices[][2])
{
	int i, j;
	
	for( i=0 ; i<2 ; i++)
	{
		for( j=0 ; j<2 ; j++)
		{
			suma = suma + matrices[i][j];
		}
	}
	printf("La suma de todos los elementos de la matriz es: %i", suma);
}

void esPrimo(int matrices[][2])
{
	int k, z, cont=1, resto=0,contPrimos=0;
	
	for( k=0 ; k<2 ; k++)
	{
		for( z=0 ; z<2 ; z++)
		{
			contPrimos=0;
			int elementosMat = matrices[k][z];
			while(cont <= elementosMat){
				resto = (elementosMat)%cont;
				printf("\nResto es: %i", resto);
				if(resto == 0)
				{
					contPrimos++;
				}
				cont++;
				
			}
			if(contPrimos == 2){
				printf("\nEl numero %i es primo.", matrices[k][z]);
			} else {
				printf("\nEl numero %i no es primo.", matrices[k][z]);
			}
			
		}
	}
}
