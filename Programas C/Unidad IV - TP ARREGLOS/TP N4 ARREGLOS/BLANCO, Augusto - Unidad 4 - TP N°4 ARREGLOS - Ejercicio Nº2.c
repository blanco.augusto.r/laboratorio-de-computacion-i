#include <stdio.h>
#include <conio.h>

//	Declaramos las funciones que vamos a utilizar para el programa
void mostrarArreglo(int arr[]);
void sumaArreglo(int arr[]);
void mayorPromedio(int arr[]);

//	Declaramos las variables globales a utilizar en todo el programa
int suma=0, promedio=0;

int main(int argc, char *argv[]) {
	//Ejercicio N�2:
	
	//Usando la declaracion de los elementos d eun arreglo, ingresamos diez numeros
	int arreglo[10] = {1,4,5,8,9,12,14,13,22,28};
	
	//Mostramos el arreglo
	mostrarArreglo(arreglo);
	printf("\n");
	//Sumamos todos los elementos del arreglo
	sumaArreglo(arreglo);
	printf("La suma de todos los arreglos es: %i", suma);
	printf("\n");
	//Hacemos el promedio del arreglo
	promedio = suma/10;
	printf("El promedio de la suma de los elementos del arreglo es: %i", promedio);
	printf("\n");
	
	//Mostramos los elementos que son mayor al promedio
	printf("Los elementos que son mayor al promedio son: ");
	mayorPromedio(arreglo);
	
	getch();
	return 0;
}

//	Funcion que muestra todos los elementos del arreglo
void mostrarArreglo(int arr[])
{
	for(int z=0 ; z<10 ; z++) {
		printf("%i ", arr[z]);
	}
	printf("\n");
}

//	Funcion que suma todos los elementos del arreglo
void sumaArreglo(int arr[]){
	for(int m=0 ; m<10 ; m++)
	{
		suma = suma + arr[m];
	}
}

//	Funcion que muestra los elementos que son mayor al promedio
void mayorPromedio(int arr[]){
	for(int k=0 ; k<10 ; k++)
	{
		if(arr[k] > promedio){
			printf("%i ", arr[k]);
		}
	}
}
