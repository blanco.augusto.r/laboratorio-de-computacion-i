#include <stdio.h>
#include <conio.h>


void mostrarArreglo(int arr[]);
void ordenamientoBurbuja(int arr[]);
void BusquedaBinaria(int arr[]);

int num;

int main(int argc, char *argv[]) {
	//Ejercicio N�4
	
	int a[10] = {2,4,6,8,10,12,14,16,18,20};
  int b[10] = {1,3,5,7,9,11,13,15,17,19};
  int c[20];
	
	int m=0, d=0, f=0;
	
	for( m=0 ; m<10 ; m++)
	{
		c[m] = a[m];
	}
	for(d=10 ; d<20 ; d++)
	{
		c[d] = b[f];
		f++;
	}
	
	mostrarArreglo(c);
	
	ordenamientoBurbuja(c);
	
	printf("\n");
	
	mostrarArreglo(c);
	
	printf("Ingrese el numero que desea buscar :"); scanf("%i", &num);
	
	BusquedaBinaria(c);
	
	getch();
	return 0;
}

//Funcion que muestra el arreglo
void mostrarArreglo(int arr[])
{
	for(int z=0 ; z<20 ; z++) {
		printf("%i ", arr[z]);
	}
	printf("\n");
}

//Funcion que ordena de menor a mayor el arreglo con el metodo "Burbuja"
void ordenamientoBurbuja(int arr[]) 
{
	int i=0, j=0, aux=0;
	
	for(i=0;i<=19;i++)
	{
		for(j=0;j<=19-1;j++)
		{
			if(arr[j]>arr[j+1])
			{
				aux=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=aux;
			}
		}
	}
}

//Algoritmo de la busqueda binaria
void BusquedaBinaria(int arr[])
{
	int inf=0, sup=20, i=0, pos=0;
	char desicion = 'f';
	
	while((inf<=sup)&&(i<20)){
		pos = (inf+sup)/2;
		if(arr[pos] == num){
			desicion = 's';
			break;
		}
		if(arr[pos]>num){
			sup = pos;
			pos = (inf+sup)/2;
		}
		if(arr[pos]<num){
			inf = pos;
			pos = (inf+sup)/2;
		}
		i++;
	}
	
	if(desicion == 's') {
		printf("\nEl valor %i coincide con un elemento del arreglo, y se encuentra en la posicion %i.", num, pos);
	} else {
		printf("\nEl valor %i no coincide con alguno de los elementos del arreglo.", num);
	}
}
