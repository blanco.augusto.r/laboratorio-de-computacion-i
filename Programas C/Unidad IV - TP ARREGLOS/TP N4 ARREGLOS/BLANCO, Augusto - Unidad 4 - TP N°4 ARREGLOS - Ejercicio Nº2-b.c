#include <stdio.h>
#include <conio.h>

// Declaramos las funciones a usar
void ordenamientoBurbuja(int arr[]);
void eliminacionDeElementosRepetidos(int arr[]);
void mostrarArreglo(int arr[]);

//	Declaramos variables locales para usar en distintas partes del programa
int contEliminacion=0, cantElim=0;

int main(int argc, char * argv[])
{
	//Ejercicio N�1
	
	int arr[10];
	int i=0, suma=0, promedio=0;
	
	//	Ingresamos diez numeros enteros en el arreglo
	printf("Ingrese 10 numeros enteros: ");
	printf("\n");
	for( i=0 ; i<10 ; i++) {
		printf("Posicion %i : ", i);
		scanf("%i", &arr[i]);
	}
	printf("\n");
	
	//	Ordenamos y mostramos el arreglo mediante funciones
	ordenamientoBurbuja(arr);
	mostrarArreglo(arr);
	
	//	Eliminamos los elementos que se repiten y solamente dejamos uno
	eliminacionDeElementosRepetidos(arr);
	
	//	Mostramos la cantidad de elementos que se elimino
	printf("La cantidad de elementos repetidos eliminados son: %i", contEliminacion);
	printf("\n");
	mostrarArreglo(arr);
	printf("\n");
	
	//	Si hubo elementos repetidos, entonces se procede a llenar esos espacios
	if(contEliminacion>0) {
		cantElim = contEliminacion;
		printf("Para que el arreglo quede completo, ingese %i numeros enteros: \n", contEliminacion);
		//	La cantidad de elementos eliminados se resta con la cantidad de elementos que contiene el arreglo, para insertar los elementos faltantes
		for(int v=10-cantElim; v<10 ; v++ )
		{
			//	Se verifica mediante un while y un if, si no se igreso un elemento que se repita con los demas elementos
			do{
				if(cantElim < contEliminacion)
				{
					printf("\n Por favor vuelva a ingresar otro numero entero a insertar en el arreglo, hasta que el numero ingresado no se repita con el resto de numeros.\n");
					contEliminacion--;
				}
				
				printf("Posicion %i: ", v);
				scanf("%i", &arr[v]);
				ordenamientoBurbuja(arr);
				eliminacionDeElementosRepetidos(arr);
			} while(cantElim < contEliminacion);
		}
	}
	
	//Suma de todos los arreglos
	for(int m=0 ; m<10 ; m++)
	{
		suma = suma + arr[m];
	}
	
	printf("\nLa el resultado de la suma de todos los elementos es: %i", suma);
	
	//	Promedio de la suma de todos los elementos del arreglo y se muestra por pantalla
	promedio = suma/10;
	printf("\nEl promedio de todos los elementos sumados es: %i", promedio);
	
	//Mostramos los elementos que son mayor al promedio
	printf("\nLos elementos que son mayor al promedio son: ");
	for(int t=0 ; t<10 ; t++)
	{
		if(arr[t] > promedio){
			printf("%i ", arr[t]);
		}
	}
	
	getch();
	return 0;
}

//Funcion que ordena de mayor a menor el arreglo con el metodo "Burbuja"
void ordenamientoBurbuja(int arr[]) 
{
	int i=0, j=0, aux=0;
	
	for(i=0;i<=9;i++)
	{
		for(j=0;j<=9-1;j++)
		{
			if(arr[j]<arr[j+1])
			{
				aux=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=aux;
			}
		}
	}
}

//	Funcion que elimina los elementos repetidos dentro del arreglo
void eliminacionDeElementosRepetidos(int arr[]) 
{
	int l=0;
	
	
	for(int k=1 ; k<10 ; k++) {
		
		if(arr[l] == arr[k])
		{
			if((arr[l]==0) && (arr[k]==0)){
				break;
			}
			arr[k] = 0;
			contEliminacion++;
			ordenamientoBurbuja(arr);
			l--;
			k--;
		}
		l++;
	}
	
	printf("\n");
}

//	Funcion que muestra el arreglo completo
void mostrarArreglo(int arr[])
{
	for(int z=0 ; z<10 ; z++) {
		printf("%i ", arr[z]);
	}
	printf("\n");
}

