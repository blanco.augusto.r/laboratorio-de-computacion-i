#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	
	int a[10] = {7,14,8,3,24,13,44,3,9,5};
	int i=0, j=0, aux=0, eleccion=0, min=0;
	char yesOrNot;
	do {
	
	printf(" *****************************************\n");
	printf("\t  ORDENAMIENTO DE ARRAYS\n");
	printf(" *****************************************\n\n");
	
	printf(" El siguiente array propuesto es: ");
	
	for( i=0 ; i<=9 ; i++) {
		printf(" %i", a[i]);
	}
	
	printf("\n\n Seleccione el tipo de ordenamiento de array:\n\n");
	printf(" \t1.Burbuja\n \t2.Seleccion Directa\n \t3.Insercion\n\n");
	printf(" Ingrese el numero de la opcion y presione enter: ");
	scanf("%i", &eleccion);
	printf("\n");
	while((eleccion != 1)&&(eleccion != 2)&&(eleccion != 3)){
		printf("\n Por favor, ingrese el numero correspondiente a las opciones dadas: ");
		scanf("%i", &eleccion);
	}
	
	switch(eleccion)
	{
	case 1:
					for(i=0;i<=9;i++){
						for(j=0;j<=9-1;j++){
							if(a[j]>a[j+1]){
								aux=a[j];
								a[j]=a[j+1];
								a[j+1]=aux;
							}
						}
					}
					for(i=0;i<=9;i++)
						{
							printf(" %i",a[i]);
						}
					break;
	case 2:
					for(i=0;i<=9;i++){
						min = i;
						for(j=i+1;j<=9;j++){
							if(a[j] < a[min]){
								min = j;
							}
						}
						aux = a[i];
						a[i] = a[min];
						a[min] = aux;
					}
					for(i=0;i<=9;i++){
						printf(" %i", a[i]);
					}
					break;
	case 3:
					for( i=0 ; i<=9 ; i++)
					{
						j=i;
						aux=a[i];
						while(j>0 && aux<a[j-1])
						{
							a[j]=a[j-1];
							j--;
						}
						a[j]=aux;
					}
					for(i=0;i<=9;i++)
					{
						printf(" %i",a[i]);
					}
					break;
	};
	printf("\n");
	printf("\n �Desea volver a elegir otra opcion del men�?\n");
	printf(" Digite la letra >> s << para seguir y presione enter.\n Digite la letra >> n << para salir y presione enter.\n ");
	printf("\n >>>> "); scanf(" %c", &yesOrNot);
	
	if (yesOrNot == 's')	system("cls");
	
	} while(yesOrNot == 's');
	
	
	return 0;
}

