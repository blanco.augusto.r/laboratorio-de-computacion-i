#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�3
	
	int i, numInsert, k, j, aux=0, min=0;
	int a[15] = {5,4,10,8,2,11,9,1};
	
	//Ingresamos el numero a insertar
	printf(" Ingrese el numero que desea insertar en el arreglo: ");
	scanf("%i", &numInsert);
	
	//Verificamos si el numero a insertar en el arreglo es mayor que 0
	while(numInsert < 0)
	{
		printf("\n Por favor, ingrese nuevamente el numero a insertar en el arreglo, recuerde que debe ser mayor que 0: ");
		scanf("%i", &numInsert);
	}
	
	//Insertamos el numero en un espacio vacio del arreglo, como son 15 espacios, lo inserte en el n� 8
	a[8] = numInsert;
	
	//Ingresamos un salto de linea para que los resultados se puedan ver con  mejor claridad
	printf("\n ");
	
	//Algoritmo de Ordenamiento por Seleccion Directa
	for( k=0 ; k<15 ; k++ ){
		min = k;
		for( j=k+1 ; j<15 ; j++ ){
			if( a[j] < a[min] ){
				min = j;
			}
		}
		aux = a[k];
		a[k] = a[min];
		a[min] = aux;
	}
	
	//Mostramos el Resultado despues de Ordenar el Arreglo
	for( i=0 ; i<15 ; i++ )
	{
		printf("%i ", a[i]);
	}
	
	getch();
	return 0;
}
