#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�4
	
	int i, numInsert, k, j, aux=0, min=0, posicion=0;
	int a[15] = {5,4,10,8,2,11,9,1,5,11,1};
	char bandera = 'f';
	//Ingresamos el numero para realizar la busqueda
	printf(" Ingrese el numero que desea buscar en el arreglo: ");
	scanf("%i", &numInsert);
	
	//Verificamos si el numero a buscar en el arreglo es mayor que 0
	while(numInsert < 0)
	{
		printf("\n Por favor, ingrese nuevamente el numero a buscar en el arreglo, recuerde que debe ser mayor que 0: ");
		scanf("%i", &numInsert);
	}
	
	//Ingresamos un salto de linea para que los resultados se puedan ver con  mejor claridad
	printf("\n ");
	
	//Algoritmo de Ordenamiento por Seleccion Directa
	for( k=0 ; k<15 ; k++ ){
		min = k;
		for( j=k+1 ; j<15 ; j++ ){
			if( a[j] < a[min] ){
				min = j;
			}
		}
		aux = a[k];
		a[k] = a[min];
		a[min] = aux;
	}
	
	//Realizamos la busqueda comun del elemento
	for( i=0 ; i<15 ; i++ )
	{
		if(a[i] == numInsert)
		{
			posicion = a[i];
			bandera = 't';
			break;
		}
	}
	
	//Mostramos el resultado por pantalla seg�n si se encontro o no el elemento
	if(bandera == 't')
	{
		printf("\n El elemento %i se encontro en la posicion n�%i del arreglo.", numInsert, posicion);
	} else {
		
		printf("\n El elemento que ingreso no se encuentra en el arreglo.");
		
	}
	
	getch();
	return 0;
}
