#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[])
{
	//Ejercicio N�2
	
	int a[8] = {1,4,6,8,10,11,15,16};
	int inf, sup, pos, num, i;
	char desicion = 'n';
	
	
	
	//Algoritmo de la Busqueda Binaria
	inf=0;
	sup=8;
	i=0;
	
	printf(" Ingrese en numero entero para verificar si coincide con el elemento dentro del arreglo: ");
	scanf("%i", &num);
	
	while(num < 0) {
		printf("\n Por favor, ingrese nuevamente, un numero que sea mayor a cero: ");
		scanf("%i", &num);
	}
	
	printf(" \n");
	
	while((inf<=sup)&&(i<8)){
		pos = (inf+sup)/2;
		if(a[pos] == num){
			desicion = 's';
			break;
		}
		if(a[pos]>num){
			sup = pos;
			pos = (inf+sup)/2;
		}
		if(a[pos]<num){
			inf = pos;
			pos = (inf+sup)/2;
		}
		i++;
	}
	
	if(desicion == 's') {
		printf("\nEl valor %i coincide con un elemento del arreglo, y se encuentra en la posicion %i.", num, pos);
	} else {
		printf("\nEl valor %i no coincide con alguno de los elementos del arreglo.", num);
	}

	getch();
	return 0;
}
