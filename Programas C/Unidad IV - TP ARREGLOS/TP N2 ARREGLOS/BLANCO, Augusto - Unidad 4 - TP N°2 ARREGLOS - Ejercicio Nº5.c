#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�5
	
	/*		Respuesta a la pregunta:
			Seg�n el enunciado, con respecto al ejercicio, se puede decir que no es necesario seguir recorriendo el arreglo,
			ya que en primera medida, se ordena el array y luego se lo recorre para buscar el elemento que coincide con el
			valor ingresado, no pide si el elemento se repite dos veces.
	*/
	
	int a[11] = {5,4,10,8,2,11,9,1,5,11,1};
	int pos=0, num, i, j=0, aux=0, k;
	char desicion = 'n';
	
	for( k=0 ; k<=10 ; k++ ){
		for(j=0;j<=10-1;j++){
			if(a[j]>a[j+1]){
				aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;
			}
		}
	}
		
	printf("\n Ingrese en numero entero para verificar si coincide con el elemento dentro del arreglo: ");
	scanf("%i", &num);
	
	while(num < 0) {
		printf("\n Por favor, ingrese nuevamente, un numero que sea mayor a cero: ");
		scanf("%i", &num);
	}
	
	printf("\n");
	
	for( i=0 ; i<=10 ; i++) 
	{
		if(num == a[i])
		{
			pos = i;
			desicion = 's';
			break;
		}
	}
	
	if(desicion == 's') {
		printf("\nEl valor %i coincide con un elemento del arreglo, y se encuentra en la posicion %i.", num, pos);
	} else {
		printf("\nEl valor %i no coincide con alguno de los elementos del arreglo.", num);
	}
	
	getch();
	return 0;
}
