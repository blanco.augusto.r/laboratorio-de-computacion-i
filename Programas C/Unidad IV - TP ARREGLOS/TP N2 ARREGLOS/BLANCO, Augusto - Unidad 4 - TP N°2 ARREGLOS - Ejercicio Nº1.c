#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�1
	
	int a[8] = {1,4,6,8,10,11,15,16};
	int pos=0, num, i;
	char desicion = 'n';
	
	printf(" Ingrese en numero entero para verificar si coincide con el elemento dentro del arreglo: ");
	scanf("%i", &num);
	
	while(num < 0) {
		printf("\n Por favor, ingrese nuevamente, un numero que sea mayor a cero: ");
		scanf("%i", &num);
	}
	
	printf(" \n");
	
	for( i=0 ; i<=7 ; i++) 
	{
		if(num == a[i])
		{
			pos = i;
			desicion = 's';
		}
	}

	if(desicion == 's') {
		printf("\nEl valor %i coincide con un elemento del arreglo, y se encuentra en la posicion %i.", num, pos);
	} else {
		printf("\nEl valor %i no coincide con alguno de los elementos del arreglo.", num);
	}

	getch();
	return 0;
}
