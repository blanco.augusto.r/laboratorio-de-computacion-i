#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[])
{
	//Ejercicio N�3.2
	
	int a[11] = {1,4,6,6,8,10,11,11,15,16,16};
	int inf, sup, pos, num, i, rep=0;
	char desicion = 'n';
	
	//Algoritmo de la Busqueda Binaria
	inf=0;
	sup=11;
	i=0;
	
	printf(" Ingrese en numero entero para verificar si coincide con el elemento dentro del arreglo: ");
	scanf("%i", &num);
	
	while(num < 0) {
		printf("\n Por favor, ingrese nuevamente, un numero que sea mayor a cero: ");
		scanf("%i", &num);
	}
	
	printf(" \n");
	
	while((inf<=sup)&&(i<11)){
		pos = (inf+sup)/2;
		if(a[pos] == num){
			desicion = 's';
			break;
		}
		if(a[pos]>num){
			sup = pos;
			pos = (inf+sup)/2;
		}
		if(a[pos]<num){
			inf = pos;
			pos = (inf+sup)/2;
		}
		i++;
		rep++;
	}
	
	if(desicion == 's') {
		printf("\nEl valor %i coincide con un elemento del arreglo, y se encuentra en la posicion %i.", num, pos);
	} else {
		printf("\nEl valor %i no coincide con alguno de los elementos del arreglo.", num);
	}
	
	if( rep>1 ) {
		printf("\nLa cantidad de veces que se repite el numero ingresado es: %i", rep);
	}
	
	getch();
	return 0;
}
