#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio N�5
	//Item: b)
	
	int num1, num2, num3;
	
	printf("Ingrese un primer valor entero: ");
	scanf("%i", &num1);
	
	printf("\nIngrese un segundo valor entero: ");
	scanf("%i", &num2);
	
	printf("\nIngrese un tercer valor entero: ");
	scanf("%i", &num3);
	
	if ((num1 == num2) && (num2 == num3)) printf("\nLos tres numeros son iguales.");
	else printf("\nLos numeros ingresados son diferentes.");
	
	getch();
	
	return 0;
}
