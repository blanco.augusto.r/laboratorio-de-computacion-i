#include <math.h>
#include <stdio.h>
#include <conio.h>


int main(int argc, char *argv[]) {
	//Ejercicio N�3
	//Item: b)
	
	double a, potencia;
	
	printf("Ingrese un numero entero cualquiera: ");
	scanf("%lf", &a);
	
	if (a > 0) 
	{
		potencia = pow(a, 2);
		printf("\nEl resultado de la potencia es: %0.lf", potencia);
	}
	else 
	{
		printf("\nEl valor ingresado es menor que cero.");
	}

	getch();	
	return 0;
}
