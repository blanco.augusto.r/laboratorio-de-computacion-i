#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	//Ejercicio Desafio
	
	srand(time(NULL));
	int piedra=1, papel=2, tijera=3, seleccionVs, jugador1, jugador2, maquina, maquina2;
	char eleccion, eleccion2;
	
	do {
	
	printf(" *********************************************\n");
	printf("\t\t <<<<JUEGO>>>>\n");
	printf("\t    Piedra, Papel y Tijera\n");
	printf(" *********************************************\n");
	printf("\n");
	
	printf(" Seleccione la opcion contra quien va a jugar: \n");
	printf("\t 1-Jugador vs Maquina\n\t 2-Jugador contra Jugador\n\t 3-Maquina contra Maquina\n");
	printf(" Digite el numero de la opcion y presione enter: ");
	scanf("%i", &seleccionVs);
	
	while(seleccionVs<1 || seleccionVs>3 ) 
	{
		printf("\n\t >>>�ERROR!<<<\n Porfavor, ingrese nuevamente los numeros que corresponen a las opciones y presione enter: ");
		scanf("%i", &seleccionVs);
	}
	
	switch(seleccionVs) 
	{
	case 1:
					do{
						
						printf("\n Jugador: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						printf(" Digite el numero de la opcion y presione enter: ");
						scanf("%i", &jugador1);
						
						while(jugador1<1 || jugador1>3 ) 
						{
							printf("\n\t >>>�ERROR!<<<\n Porfavor, digite nuevamente los numeros que corresponen a las opciones y presione enter: ");
							scanf("%i", &jugador1);
						}
						
						printf("\n Maquina: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						maquina =rand()%3+1;
						printf(" Maquina elije la opcion: %i\n",maquina);
						
						if(jugador1==tijera && maquina==papel)	printf("\n >>> �El jugador gana! Tijeras corta papel.<<<\n");
						else if(jugador1==papel && maquina==piedra) printf("\n >>> �El jugador gana! Papel envuelve a piedra.<<<\n");
						else if(jugador1==piedra && maquina==tijera) printf("\n >>> �El jugador gana! Piedra rompe tijera.<<<\n");
						else if(maquina==tijera && jugador1==papel) printf("\n >>> �La maquina gana! Tijeras corta papel.<<<\n");
						else if(maquina==papel && jugador1==piedra)	printf("\n >>> �La maquina gana! Papel envuelve a piedra.<<<\n");
						else if(maquina==piedra && jugador1==tijera) printf("\n >>> �La maquina gana! Piedra rompe tijera.<<<\n");
						else printf("\n\t >>>�EMPATE!<<<\n");
						
						printf("\n\t �Desea jugar de nuevo?\n");
						printf(" Digite la letra >> s << para seguir y presione enter.\n Digite la letra >> n << para salir y presione enter.\n ");
						scanf(" %c", &eleccion);
						} while(eleccion == 's');
					break;
	case 2:
					do{
						
						printf("\n Jugador1: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						printf(" Digite el numero de la opcion y presione enter: ");
						scanf("%i", &jugador1);
						
						while(jugador1<1 || jugador1>3 ) 
						{
							printf("\n\t >>>�ERROR!<<<\n Porfavor, digite nuevamente los numeros que corresponen a las opciones y presione enter: ");
							scanf("%i", &jugador1);
						}
						
						printf("\n");
						
						printf("\n Jugador2: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						printf(" Digite el numero de la opcion y presione enter: ");
						scanf("%i", &jugador2);
						
						while(jugador2<1 || jugador2>3 ) 
						{
							printf("\n\t >>>�ERROR!<<<\n Porfavor, digite nuevamente los numeros que corresponen a las opciones y presione enter: ");
							scanf("%i", &jugador2);
						}
						
						if(jugador1==tijera && jugador2==papel)	printf("\n >>> �El jugador1 gana! Tijeras corta papel.<<<\n");
						else if(jugador1==papel && jugador2==piedra) printf("\n >>> �El jugador1 gana! Papel envuelve a piedra.<<<\n");
						else if(jugador1==piedra && jugador2==tijera) printf("\n >>> �El jugador1 gana! Piedra rompe tijera.<<<\n");
						else if(jugador2==tijera && jugador1==papel) printf("\n >>> �El jugador2 gana! Tijeras corta papel.<<<\n");
						else if(jugador2==papel && jugador1==piedra)	printf("\n >>> �El jugador2 gana! Papel envuelve a piedra.<<<\n");
						else if(jugador2==piedra && jugador1==tijera) printf("\n >>> �El jugador2 gana! Piedra rompe tijera.<<<\n");
						else printf("\n\t >>>�EMPATE!<<<\n");
						
						printf("\n\t �Desea jugar de nuevo?\n");
						printf(" Digite la letra >> s << para seguir y presione enter.\n Digite la letra >> n << para salir y presione enter.\n ");
						scanf(" %c", &eleccion);
						} while(eleccion == 's');
					break;
	case 3:
					do{
						
						printf("\n Maquina1: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						maquina =rand()%3+1;
						printf(" Maquina1 elije la opcion: %i\n",maquina);
						
						printf("\n Maquina2: elija la opcion con la que desea jugar: ");
						printf("\n\t 1-Piedra.\n\t 2-Papel.\n\t 3-Tijera.\n");
						maquina2 =rand()%3+1;
						printf(" Maquina2 elije la opcion: %i\n",maquina2);
						
						if(maquina==tijera && maquina2==papel)	printf("\n >>> �La maquina1 gana! Tijeras corta papel.<<<\n");
						else if(maquina==papel && maquina2==piedra) printf("\n >>> �La maquina1 gana! Papel envuelve a piedra.<<<\n");
						else if(maquina==piedra && maquina2==tijera) printf("\n >>> �La maquina1 gana! Piedra rompe tijera.<<<\n");
						else if(maquina2==tijera && maquina==papel) printf("\n >>> �La maquina2 gana! Tijeras corta papel.<<<\n");
						else if(maquina2==papel && maquina==piedra)	printf("\n >>> �La maquina2 gana! Papel envuelve a piedra.<<<\n");
						else if(maquina2==piedra && maquina==tijera) printf("\n >>> �La maquina2 gana! Piedra rompe tijera.<<<\n");
						else printf("\n\t >>>�EMPATE!<<<\n");
						
						printf("\n\t �Desea jugar de nuevo?(s/n)\n");
						printf(" Digite la letra >> s << para seguir y presione enter.\n Digite la letra >> n << para salir y presione enter.\n ");
						scanf(" %c", &eleccion);
						} while(eleccion == 's');
					break;
	}
	
	printf("\n\t �Desea elegir otra opcion para jugar de nuevo?(s/n)\n");
	printf(" Digite la letra >> s << para seguir y presione enter.\n Digite la letra >> n << para salir y presione enter.\n ");
	scanf(" %c", &eleccion2);
	
	if(eleccion2 == 's')system("cls");
	
	} while(eleccion2 == 's');
	
	return 0;
}
