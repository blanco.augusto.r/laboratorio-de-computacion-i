#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) {
	//Ejercicio N�3
	//Item: a)
	
	int a, b, div;
	
	printf("Ingrese un primer valor entero: ");
	scanf("%i", &a);
	
	printf("\nIngrese un segundo valor entero: ");
	scanf("%i", &b);
	
	div=a%b;
	
	if (b < a) {
		if (div == 0) {
			printf("\nEl numero %i es divisible por el numero %i.", a,b);
		}
	} 
	else 
	{
		printf("\nLos numeros ingresados no son divisibles.");
	}
	
	getch();
	
	return 0;
}
