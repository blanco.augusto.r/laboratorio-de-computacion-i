#include <stdio.h>
#include <conio.h>

int main(int argc, char *argv[]) 
{
	//Ejercicio 2
	
	int i = 0, x = 0;
	
	printf("Cuantos numeros dentro del intervalo son divisibles por cinco: \n");
	
	do {
		
		if (i % 5 == 0) 
		{
			x++;
			printf("\n%d", x);
		}
		
		++i;
	}while(i < 20);
	
	printf("\nx = %d", x);
	
	getch();
	return 0;
}
